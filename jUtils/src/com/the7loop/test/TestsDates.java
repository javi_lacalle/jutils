package com.the7loop.test;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Test;

import com.the7loop.utils.Dates;

public class TestsDates {


	/****************************************************************************/
	/*****************     ConvertStringToSQLDate      **************************/
	/****************************************************************************/
	
	@Test
	public void testConvertStringToSQLDateCorrect1() throws Exception {
		String fecha1= "13/10/1988";
		Date expect = Date.valueOf("1988-10-13");
		assertEquals(expect,Dates.ConvertStringToSQLDate(fecha1, "/"));
	}
	
	@Test
	public void testConvertStringToSQLDateCorrect2() throws Exception {
		String fecha1= "13-10-1988";
		Date expect = Date.valueOf("1988-10-13");
		assertEquals(expect,Dates.ConvertStringToSQLDate(fecha1, "-"));
	}
	
	@Test(expected = Exception.class)
	public void testConvertStringToSQLDateIncorrectFormat() throws Exception {
		String fecha1= "10/13/1988";
		Date expect = Date.valueOf("1988-10-13");
		assertEquals(expect,Dates.ConvertStringToSQLDate(fecha1, "/"));
	}
	
	@Test(expected = Exception.class)
	public void testConvertStringToSQLDateIncorrectSeparator() throws Exception {
		String fecha1= "13/10/1988";
		Date expect = Date.valueOf("1988-10-13");
		assertEquals(expect,Dates.ConvertStringToSQLDate(fecha1, "-"));
	}

	
	/****************************************************************************/
	/*****************ConvertStringDateToStringDateEEUU**************************/
	/****************************************************************************/
	
	
	@Test
	public void testConvertStringDateToStringDateEEUUCorrect1() throws Exception {
		String fecha1= "13/10/1988";
		String expected = "10-13-1988";
		assertEquals(expected,Dates.ConvertStringDateToStringDateEEUU(fecha1, "/", "-"));
	}
	
	@Test
	public void testConvertStringDateToStringDateEEUUCorrect2() throws Exception {
		String fecha1= "13-10-1988";
		String expected = "10-13-1988";
		assertEquals(expected,Dates.ConvertStringDateToStringDateEEUU(fecha1, "-", "-"));
	}
	
	@Test(expected = Exception.class)
	public void testConvertStringDateToStringDateEEUUIncorrectSeparator() throws Exception {
		String fecha1= "13-10-1988";
		String expected = "10-13-1988";
		assertEquals(expected,Dates.ConvertStringDateToStringDateEEUU(fecha1, "/", "-"));
	}


	/****************************************************************************/
	/*****************           CompareDates          **************************/
	/*****************************************************************************/
	
	
	@Test
	public void testCompareDatesCorrect1() throws Exception {
		Date date1 = Date.valueOf("1988-10-13");
		Date date2 = Date.valueOf("2000-10-13");
		assertEquals(-1, Dates.CompareDates(date1, date2));
	}
	
	@Test
	public void testCompareDatesCorrect2() throws Exception {
		Date date1 = Date.valueOf("1988-10-13");
		Date date2 = Date.valueOf("1988-10-13");
		assertEquals(0, Dates.CompareDates(date1, date2));
	}
	
	@Test
	public void testCompareDatesCorrect3() throws Exception {
		Date date1 = Date.valueOf("1988-10-13");
		Date date2 = Date.valueOf("2000-10-13");
		assertEquals(1, Dates.CompareDates(date2, date1));
	}
	

	/*****************************************************************************/
	/*****************  CompareInternationalStringDates  *************************/
	/*****************************************************************************/
	
	
	@Test
	public void testCompareInternationalStringDatesSuperior()
	{
		String date1 = "13/10/1988";
		String date2 = "13/10/2013";
		assertEquals(1, Dates.CompareInternationalStringDates(date1, date2, "/"));
	}
	
	@Test
	public void testCompareInternationalStringDatesInferior()
	{
		String date1 = "13/10/1988";
		String date2 = "13/10/2013";
		assertEquals(-1, Dates.CompareInternationalStringDates(date2, date1, "/"));
	}
	
	@Test
	public void testCompareInternationalStringDatesIguales()
	{
		String date1 = "13/10/2013";
		String date2 = "13/10/2013";
		assertEquals(0, Dates.CompareInternationalStringDates(date1, date2, "/"));
	}
	
	@Test
	public void testCompareInternationalStringDatesDiaMas()
	{
		String date1 = "13/10/2013";
		String date2 = "14/10/2013";
		assertEquals(1, Dates.CompareInternationalStringDates(date1, date2, "/"));
	}
	
	@Test
	public void testCompareInternationalStringDatesDiaMenos()
	{
		String date1 = "13/10/2013";
		String date2 = "12/10/2013";
		assertEquals(-1, Dates.CompareInternationalStringDates(date1, date2, "/"));
	}
	
	@Test
	public void testCompareInternationalStringDatesMesMas()
	{
		String date1 = "13/10/2013";
		String date2 = "14/11/2013";
		assertEquals(1, Dates.CompareInternationalStringDates(date1, date2, "/"));
	}
	
	@Test
	public void testCompareInternationalStringDatesMesMenos()
	{
		String date1 = "13/10/2013";
		String date2 = "12/9/2013";
		assertEquals(-1, Dates.CompareInternationalStringDates(date1, date2, "/"));
	}

	/*****************************************************************************/
	/*****************  NumberOfDaysBetweenTwoDates  *************************/
	/** ************************************************************************/
	
	@Test
	public void testNumberDaysBetweenTwoDates() throws Exception
	{
		Date fecha1 = Date.valueOf("2013-10-10");
		Date fecha2 = Date.valueOf("2013-10-16");
		assertEquals(6, Dates.NumberOfDaysBetweenTwoDates(fecha1, fecha2));
	}
	
	
	
	public void testCalculateDateDaysLater() throws Exception {
		Date fecha1 = Date.valueOf("2013-10-20");
		assertEquals(0, Dates.CompareDates(fecha1, Dates.CalculateDateDaysLater(4)));
	}
	
}
