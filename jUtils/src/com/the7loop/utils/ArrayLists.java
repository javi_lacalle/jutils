package com.the7loop.utils;

import java.util.ArrayList;

public class ArrayLists {

	public static ArrayList<Object> MergeFirst(ArrayList<Object> array, Object one, Object two )
	{
		boolean forOne = false;
		boolean forTwo = false;
		try {
			for (int i = 0; i < array.size() && !forOne; i++) {
				if(array.get(i).equals(one)) {
					
					for (int j = 0; j < array.size() && !forTwo; j++) {
						if(array.get(j).equals(two)) {
							array.set(i, two);
							array.set(j, one);
							
							forTwo = true; //Condition of exit "for" two
						}
					}
					forOne = true; //Condition of exit "for" one
				}
			}
			
			
		} catch (Exception e) {
			throw e;
			
		}
		return array;
	}
	
	public static ArrayList<Object> MergeAll(ArrayList<Object> array, Object one, Object two )
	{
		boolean forTwo = false;
		try {
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).equals(one)) {
					
					for (int j = 0; j < array.size() && !forTwo; j++) {
						if(array.get(j).equals(two)) {
							array.set(i, two);
							array.set(j, one);
							
							forTwo = true; //Condition of exit "for" two
						}
					}
				}
			}
			
			
		} catch (Exception e) {
			throw e;
			
		}
		return array;
	}

}
