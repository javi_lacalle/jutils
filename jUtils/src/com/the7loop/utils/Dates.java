package com.the7loop.utils;

import java.sql.Date;

public class Dates {

	/**
	 * ConvertStringToSQLDate
	 * 	Convert a String date with a specific separator in a SQL Date.
	 * 	String format e.g. "dd/mm/yyyy"
	 * 
	 * @param date
	 * @param separator
	 * @return
	 * @throws Exception 
	 */
	public static Date ConvertStringToSQLDate(String date,String separator) throws Exception
	{
		Date finalDate = new Date(System.currentTimeMillis());
		String[] temp = null;
		try {
			temp=date.split(separator);
			String finalString=temp[2]+"-"+temp[1]+"-"+temp[0];
			finalDate= Date.valueOf(finalString);
			
		} catch (Exception e) {
			
			if(temp!=null && temp[0].equals(date))
			{
				throw new Exception("Separator incorrect for String date");
				
			} else {
				throw new Exception("Incorrect String date format.");
			}
		}
		
		return finalDate;
	}
	
	/**
	 * ConvertStringDateToStringDateEEUU
	 * 	Transform a international String format date to EEUU format with a expecific separator.
	 * 	e.g international format: dd/MM/yyyy  ; EEUU format: MM/dd/yyyy
	 *	Works correctly with separator '/' and '-'
	 * 
	 * @param dateInternational
	 * @param inSepartor
	 * @param outSeparator
	 * @return
	 * @throws Exception 
	 */
	public static String ConvertStringDateToStringDateEEUU(String dateInternational,String inSeparator, String outSeparator) throws Exception
	{
		String dateEEUU = null;
		try {
			String[] temp=dateInternational.split(inSeparator);
			dateEEUU = temp[1]+outSeparator+temp[0]+outSeparator+temp[2];
		} catch (Exception e) {
			throw new Exception("Separator incorrect for String dateInternational");
		}
		return dateEEUU;
	}
	
	/**
	 * CompareDates
	 * 	Compare two dates.
	 * 	Return -1 if date1>date2
	 * 	Return 0 if date1==date2
	 * 	Return 1 if date1<date2
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 * @throws Exception
	 */
	public static int CompareDates(Date date1, Date date2) throws Exception {
		int compare = 0;
		
		try {
			int c = date1.compareTo(date2);
			if(c<0)	{
				compare = -1;
				
			} else if(c>0) {
				compare = 1;
			} else {
				compare = 0;
			}
		} catch (Exception e) {
			throw e;
		}
		
		return compare;
	}
	
	/**
	 * CompareInternationalStringDates
	 * 	Compare two String Dates with a specific separator.
	 * 	e.g. dd/mm/yyyy;
	 * 	Return -1 if date1>date2
	 * 	Return 0 if date1==date2
	 * 	Return 1 if date1<date2
	 * 
	 * @param date1
	 * @param date2
	 * @param separator
	 * @return
	 */
	public static int CompareInternationalStringDates(String date1,String date2,String separator)	{
		int compare = 0;
		String[] temp1 = null;
		String[] temp2 = null;
		
		try {
			temp1=date1.split(separator);
			temp2=date2.split(separator);
			
			//First Compare Years
			if(Integer.valueOf(temp1[2]) <= Integer.valueOf(temp2[2])) // Date1 Year less or equals
			{
				if(Integer.valueOf(temp1[2]).equals(Integer.valueOf(temp2[2]))) { //Same year
					
					//Compare Months
					if(Integer.valueOf(temp1[1])<=Integer.valueOf(temp2[1])) { //Date1 Month less or equals
						if(Integer.valueOf(temp1[1]).equals(Integer.valueOf(temp2[1]))) { //Same months
							
							//Compare Days
							if(Integer.valueOf(temp1[0])<=Integer.valueOf(temp2[0])) { //Date1 Day less or equals
								if(Integer.valueOf(temp1[0]).equals(Integer.valueOf(temp2[0]))) { //Same day
									compare = 0;
								} else { //Date1 More day
									compare = 1;
								}
								
							} else { //Date1 Less day
								compare = -1;
							}
							
						} else { //Date1 More month
							compare =1;
						}
						
					}else { //Date1 More Month
						compare = -1;
					}
					
					
				} else { //Date1 Less Year
					compare = 1;
				}
				
			} else { // Date1 More Year
				compare = -1;
			}
		} catch (Exception e) {
			throw e;
		}
		
		return compare;
	}
	
	/**
	 * NumberOfDaysBetweenTwoDates
	 * 	Return the numbers of days between two dates in sql format.
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 * @throws Exception
	 */
	public static int NumberOfDaysBetweenTwoDates(Date date1,Date date2) throws Exception {
		int days = 0;
		try {
			long miliseconsInDay = 24*60*60*1000;
			long miliseconsBetween = date2.getTime()-date1.getTime();
			days = (int) (miliseconsBetween/miliseconsInDay);
		} catch (Exception e) {
			throw new Exception("Incorrect operation");
		}
		
		return days;
	}
	
	
	/**
	 * CalculateDateDaysLater
	 * 	Calculate a Date a number of days later.
	 * 	e.g. today + 5 days.
	 * 
	 * 
	 * @param numberOfDays
	 * @return
	 * @throws Exception 
	 */
	public static Date CalculateDateDaysLater(int numberOfDays) throws Exception {
		Date date = null;;
		try {
			if(numberOfDays>=0) {
				long daysInMilisecons = numberOfDays*24*60*60*1000;
				date = new Date(System.currentTimeMillis()+daysInMilisecons);
			}
			else {
				throw new Exception("NumberOfDays sould be positive");
			}
		} catch (Exception e) {
			throw e;
		}
		
		return date;
	}
	
	/**
	 * CalculateDateDaysBefore
	 * 	Calculate a Date a number of days before
	 * 
	 * @param numberOfDays
	 * @return
	 * @throws Exception
	 */
	public static Date CalculateDateDaysBefore(int numberOfDays) throws Exception {
		Date date = null;;
		try {
			if(numberOfDays>=0) {
				long daysInMilisecons = numberOfDays*24*60*60*1000;
				date = new Date(System.currentTimeMillis()-daysInMilisecons);
			}
			else {
				throw new Exception("NumberOfDays sould be positive");
			}
		} catch (Exception e) {
			throw e;
		}
		
		return date;
	}
	

}
