package com.the7loop.utils;

public class Numerics {

	/**
	 * IsInteger
	 * 	Return true if param String is a integer. False in other case.
	 * 
	 * @param s
	 * @return
	 */
	public static boolean IsInteger(String s) {
	    return IsInteger(s,10);
	}

	/**
	 * IsInteger
	 * 	Return true if param String is a integer. False in other case.
	 * 
	 * @param s
	 * @param radix
	 * @return
	 */
	public static boolean IsInteger(String s, int radix) {
	    if(s.isEmpty()) return false;
	    for(int i = 0; i < s.length(); i++) {
	        if(i == 0 && s.charAt(i) == '-') {
	            if(s.length() == 1) return false;
	            else continue;
	        }
	        if(Character.digit(s.charAt(i),radix) < 0) return false;
	    }
	    return true;
	}

}
