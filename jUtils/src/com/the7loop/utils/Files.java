package com.the7loop.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Files {

	/**
	 * No es funcional es solo un ejemplo de código.
	 */
	void LeerFichero() {
		File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;
	 
	      try {
	         // Apertura del fichero y creacion de BufferedReader para poder
	         // hacer una lectura comoda (disponer del metodo readLine()).
	         archivo = new File ("C:\\archivo.txt");
	         fr = new FileReader (archivo);
	         br = new BufferedReader(fr);
	 
	         // Lectura del fichero
	         String linea;
	         while((linea=br.readLine())!=null)
	            System.out.println(linea);
	      }
	      catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el fichero, para asegurarnos
	         // que se cierra tanto si todo va bien como si salta 
	         // una excepcion.
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
	}
	
	
	/**
	 * No es funcional es un ejemplo
	 */
	void EscribirFichero() {
		FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("c:/prueba.txt");
            pw = new PrintWriter(fichero);
 
            for (int i = 0; i < 10; i++)
            {
                pw.println("Linea " + i);
            }
 
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
	}

	
	/*******************************************************************************/
	/**********************        Ficheros Binarios      **************************/
	/*******************************************************************************/
	
	/**
	 *  Copiar un fichero binario en otro destino
	 * @param dir1
	 * @param dir2
	 */
	void CopiaFicherosBinarios(String dir1, String dir2) {
        copia ("c:/ficheroOrigen.bin", "c:/ficheroDestino.bin");
        
	}

	
	public static void copia (String ficheroOriginal, String ficheroCopia)
    {
        try
        {
                        // Se abre el fichero original para lectura
            FileInputStream fileInput = new FileInputStream(ficheroOriginal);
            BufferedInputStream bufferedInput = new BufferedInputStream(fileInput);
             
            // Se abre el fichero donde se hará la copia
            FileOutputStream fileOutput = new FileOutputStream (ficheroCopia);
            BufferedOutputStream bufferedOutput = new BufferedOutputStream(fileOutput);
             
            // Bucle para leer de un fichero y escribir en el otro.
            byte [] array = new byte[1000];
            int leidos = bufferedInput.read(array);
            while (leidos > 0)
            {
                bufferedOutput.write(array,0,leidos);
                leidos=bufferedInput.read(array);
            }
 
            // Cierre de los ficheros
            bufferedInput.close();
            bufferedOutput.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

	/*******************************************************************************/
	/**********************    Listar Ficheros Directorio **************************/
	/*******************************************************************************/
	
	
	void listarFicheros(String directorio) {
		File dir = new File(directorio);
		String[] ficheros = dir.list();
		
		//Importante , si no abre el fichero o no hay ficheros devuelve NULL
		if (ficheros == null)
			  System.out.println("No hay ficheros en el directorio especificado");
			else { 
			  for (int x=0;x<ficheros.length;x++)
			    System.out.println(ficheros[x]);
			}
	}
	
}
