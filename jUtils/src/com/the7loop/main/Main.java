/**
 * 
 */
package com.the7loop.main;


import java.util.ArrayList;

import com.the7loop.utils.ArrayLists;
import com.the7loop.utils.Strings;

/**
 * @author jld9
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Hello world! This is my first java Library");
		
		try {
			ArrayList<Object> lista = new ArrayList<>();
			ArrayList<Object> aux = new ArrayList<>();
			ArrayList<Object> aux2 = new ArrayList<>();
			for (Integer i = 0; i < 5; i++) {
				lista.add(i);
			}
			lista.add(2);
			
			
			System.out.println("-------------------Pruebas ArrayList-------------------");
			System.out.println("Sin modificiar:  "+lista);
			aux = ArrayLists.MergeFirst(lista, 2, 4);
			System.out.println("MergeFirt - 2 por 4: "+aux);
			aux2 = ArrayLists.MergeAll(lista, 2, 4);
			System.out.println("MergeAll - 2 por 4: "+aux2);
			
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		

	}

}
